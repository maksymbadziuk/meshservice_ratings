DROP TABLE IF EXISTS ratings;

CREATE TABLE ratings (
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  rating DECIMAL(7,3) NOT NULL,
  presentation VARCHAR(250) NOT NULL
);

INSERT INTO ratings (rating, presentation) VALUES
(455.45, ':-)'),
(666.4, ':-)'),
(111.2, ':-)'),
(123.4, ':-)'),
(432.4, ':-)'),
(532.4, ':-)');