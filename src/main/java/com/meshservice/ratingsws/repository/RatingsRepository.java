package com.meshservice.ratingsws.repository;

import com.meshservice.ratingsws.entity.Rating;
import org.springframework.data.repository.CrudRepository;

public interface RatingsRepository extends CrudRepository<Rating, Long> {
}
