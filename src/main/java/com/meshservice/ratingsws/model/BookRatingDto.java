package com.meshservice.ratingsws.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
public class BookRatingDto {

    private Long id;
    private Double rating;
    private String presentation;
}
