package com.meshservice.ratingsws.controller;

import com.meshservice.ratingsws.model.BookRatingDto;
import com.meshservice.ratingsws.service.RatingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BooksRatingsController {

    Logger log = LoggerFactory.getLogger(BooksRatingsController.class);

    @Autowired
    private RatingsService ratingsService;


    @CrossOrigin
    @GetMapping(value="/")
    public String getRoot() {
        return "IP Address is: ";
    }

    @GetMapping(value = "/ratings/{ids}")
    public List<BookRatingDto> getAllBooksRatings(final @PathVariable List<Long> ids) {

        log.info(ids.toString());

        return ratingsService.getBooksRatingsByIds(ids);

    }
}
