package com.meshservice.ratingsws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class RatingsWsApplication {

    public static void main(String[] args) {
        SpringApplication.run(RatingsWsApplication.class, args);
    }

}
