package com.meshservice.ratingsws.service;

import com.meshservice.ratingsws.model.BookRatingDto;
import com.meshservice.ratingsws.repository.RatingsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class RatingsService {

    Logger log = LoggerFactory.getLogger(RatingsService.class);

    @Autowired
    private RatingsRepository ratingsRepository;

    public List<BookRatingDto> getBooksRatingsByIds(final List<Long> ids) {
        return StreamSupport.stream(ratingsRepository.findAllById(ids).spliterator(), false)
                .map(ratingEntity -> new BookRatingDto(ratingEntity.getId(), ratingEntity.getRating(), ratingEntity.getPresentation()))
                .collect(Collectors.toList());
    }
}
