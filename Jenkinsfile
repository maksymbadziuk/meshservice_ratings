pipeline {

    agent any

    environment {
        SVN_BUILD_VERSION = "-Dsvn.version=${env.BUILD_NUMBER}-SNAPSHOT"
        APP_NAME = "books-ratings-app"
        IMAGE_BUILD_VERSION="${env.BUILD_NUMBER}"
        AWS_REGION='eu-central-1'
    }

    stages {

      stage('Build') {
        steps {
              withMaven(maven: 'jenkins-maven') {
                   sh 'mvn clean package -DskipTests ${SVN_BUILD_VERSION}'
                 }
        }

            post {
                success {
                   archiveArtifacts artifacts: 'target/*.jar', fingerprint: true
                 }
           }
     }

    stage('Test') {

            steps {

                withMaven(maven: 'jenkins-maven') {
                    sh 'mvn test ${SVN_BUILD_VERSION}'
                }
            }

            post {
                always {
                    junit 'target/surefire-reports/*.xml'
                }
            }
        }

    stage('Image') {
        steps {
            sh '''
                AWS_ACCOUNT_ID=$(aws sts get-caller-identity --query "Account" --output text)
                AWS_ECR_REPO_LINK=$AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com
                AWS_FULL_CONTAINER_TAG=$AWS_ECR_REPO_LINK/$APP_NAME:$IMAGE_BUILD_VERSION
                echo 'Container TAGE - $AWS_FULL_CONTAINER_TAG'
                docker build -t $AWS_FULL_CONTAINER_TAG .
            '''
        }
    }

    stage('Push') {
        steps {
            sh '''
                AWS_ACCOUNT_ID=$(aws sts get-caller-identity --query "Account" --output text)
                AWS_ECR_REPO_LINK=$AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com
                AWS_FULL_CONTAINER_TAG=$AWS_ECR_REPO_LINK/$APP_NAME:$IMAGE_BUILD_VERSION
                aws ecr get-login-password \
                      --region $AWS_REGION | docker login \
                      --username AWS \
                      --password-stdin $AWS_ECR_REPO_LINK

                docker push $AWS_FULL_CONTAINER_TAG

            '''
        }
    }


    stage('Deploy') {
        steps {
            sh '''
                cd /var/lib/jenkins/workspace/books-terraform-ecs/live/test
                terraform init
                terraform refresh
                terraform apply -target aws_ecs_service.books-ratings-app-service -var BOOKS_RATINGS_APP_SERVICE_ENABLE="1" -var BOOKS_RATINGS_APP_VERSION=$IMAGE_BUILD_VERSION -var TASK_DESIRED_COUNT="2" -auto-approve
            '''
        }
    }

   }
}